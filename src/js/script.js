'use strict';

// Variables
var viewer = el('#viewer'),
    equals = el('#equals'),
    clear = el('#clear'),
    equationEl = el('#equation'),
    nums = el('.num'),
    ops = el('.ops'),
    theNum = '',
    oldNum = '',
    resultNum,
    equation,
    port = 'javademo',
    operator;

function el(element) {
    if (element.charAt(0) === '#') {
        return document.querySelector(element);
    }

    return document.querySelectorAll(element);
}

function clearAll() {
    oldNum = '';
    theNum = '';
    viewer.innerHTML = '0';
    equation = '';
    equationEl.innerHTML = '';
    equals.setAttribute('data-result', resultNum);
}

function swapPort(el) {
    const language = el.options[el.selectedIndex].value;

    switch (language) {
        case 'Java':
            port = 'javademo';
            break;
        case 'Node':
            port = 'nodedemo';
            break;
        case 'Python':
            port = 'pythondemo';
            break;
    }
}

function moveNum(op) {
    oldNum = theNum;
    theNum = '';
    operator = op.getAttribute('data-ops');
    op.blur();

    equals.setAttribute('data-result', ''); // Reset result in attr
}

function setNum(num) {
    if (resultNum) { // If a result was displayed, reset number
        theNum = num.getAttribute('data-num');
        resultNum = '';
    } else { // Otherwise, add digit to previous number (this is a string!)
        theNum += num.getAttribute('data-num');
    }

    num.blur();

    viewer.innerHTML = theNum; // Display current number
}

// When: Equals is clicked. Calculate result
async

function displayNum(eq) {
    // Convert string input to numbers
    oldNum = parseFloat(oldNum);
    theNum = parseFloat(theNum);

    await
    req(operator, oldNum, theNum);

    // Display result, finally!
    viewer.innerHTML = resultNum;
    equationEl.innerHTML = equation;
    equals.setAttribute('data-result', resultNum);

    // Now reset oldNum & keep result
    oldNum = 0;
    theNum = resultNum;

    eq.blur();
}

async

function req(operator, oldNum, theNum) {
    await
    axios.get('https://' + port + '.bbn.nerd.vision' + '/' + operator + '?int1=' + oldNum + '&int2=' + theNum)
        .then(function (response) {
            resultNum = response.data.result;
            equation = response.data.equation;
        })
        .catch(function () {
            resultNum = 'ERROR';
            equation = '';
            equationEl.innerHTML = '';
        });
}
