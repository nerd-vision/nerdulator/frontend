FROM nginx

COPY docker/nginx.conf /etc/nginx/conf.d/default.conf

COPY src/ /usr/share/nginx/html