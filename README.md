![alt text](src/img/NV_Readme.jpg "nerd.vision")

[Get started with nerd.vision for free](https://nerd.vision)

# Nerdulator

This is the front end for the Nerdulator nerd.vision demo calculator app.

## Build

To build simply build the dockerfile.

```bash
docker build -t nerdulator/frontend .
```

## Run

To run use the docker-compose file, after it has been build.

```bash
docker-compose up
```

